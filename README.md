# Flectra Community / crm

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[crm_security_group](crm_security_group/) | 2.0.1.0.1| Add new group in Sales to show only CRM
[crm_claim](crm_claim/) | 2.0.1.0.1| Track your customers/vendors claims and grievances.
[crm_location](crm_location/) | 2.0.1.0.0| CRM location
[crm_lead_code](crm_lead_code/) | 2.0.1.0.0| Sequential Code for Leads / Opportunities
[crm_lead_firstname](crm_lead_firstname/) | 2.0.1.0.2| Specify split names for contacts in leads
[crm_phonecall](crm_phonecall/) | 2.0.1.0.1| CRM Phone Calls
[crm_lead_vat](crm_lead_vat/) | 2.0.1.1.0| Add VAT field to leads


